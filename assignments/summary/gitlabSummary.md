**GitLab** is a service which provides UI to manage git repositories and the 2nd most popular in git services after Github. 

It has the following features:

1. It is free to use
2. It has a user-newbie-friendly interface
3. Contains both public and private repositories
4. Has all functions of git like cloning, forking, merge, branches, push, pull, etc.
5. Repositories can be worked on both locally as well as on the site UI itslef
6. It features wiki, issue-tracking and continuous integration/deployment pipeline features
7. It is great for team work/projects where all code and input can be compiled in a single place
8. The UI has various features in which you can style and design your write-up 

