**What is Git?**

Git is basically a version control software.
We can keep track of all the changes made by us or anyone else working with us on the same project.
It helps very much in team projects.

**Concepts in Git**

*Repository* : It is the collection of all the files and folders related to that project. It is where everything is stored and all the team members add their code and work.

*Commit* : It is a way to store a copy of a file in that immediate moment. One can always traceback to this copy if he or dhe feels that there is readjustment needed. This copy only exists on the local repository until we push it into the remote one.

*Branch* : This is like a separate instance of the code from the main database. If the repository is considered a tree then the *Master branch* is the trunk and the rest are its branches.

*Push* : It syncs the local repo to the remote repo.

*Merge* : It exactly means merging two braches together. It is used mostly when you are satisfied that the changes you have made in your branch are correct and error free, and want those features added to your master branch.

*Clone* : It is used to clone the enitre repo to your local machine.

*Fork* : Instead of creating a copy like in cloning, forking creates an enitire new repo with the same material under your name.

**Normal Procedure in Git Workflow :**

-> Clone the repo.
-> Create new branch and select it/use the master branch only.
-> Add the files from your local repo which you are sure you want to save.
-> Commit the changes to save to your Local Repo.
-> Push the changes to Remote repo once you are sure the changes are right and error-free.
